# bravo_utilities

Collection of scripts for working with the Bravo Arm.

## Installation
Most of these require [pybravo](https://github.com/Robotic-Decision-Making-Lab/pybravo). Follow the installation instructions for a local pip installation.

## Usage

See the individual folders for usage instructions.